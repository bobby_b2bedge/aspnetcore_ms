FROM mcr.microsoft.com/dotnet/core/aspnet:2.1-stretch-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443
ENV REDIS_SERVERS "127.0.0.1:6379"

FROM mcr.microsoft.com/dotnet/core/sdk:2.1-stretch AS build
WORKDIR /src
COPY ["ASPNetCore_MicroService/ASPNetCore_MicroService.csproj", "ASPNetCore_MicroService/"]
RUN dotnet restore "ASPNetCore_MicroService/ASPNetCore_MicroService.csproj"
COPY . .
WORKDIR "/src/ASPNetCore_MicroService"
RUN dotnet build "ASPNetCore_MicroService.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ASPNetCore_MicroService.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ASPNetCore_MicroService.dll"]