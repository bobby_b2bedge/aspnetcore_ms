﻿using System;
using ASPNetCore_MicroService.Models;
using StackExchange.Redis;

/// <summary>
/// Summary description for Class1
/// </summary>
/// 

namespace ASPNetCore_MicroService
{
	public class RedisStreams
	{
        readonly ConnectionMultiplexer redis;
		IDatabase db;


		//Initiate a connect to the redis server(s)
		public RedisStreams(String ipList)
		{
			redis = ConnectionMultiplexer.Connect(ipList);
		}

		//connect to the Redis database
		public void InitStreams()
		{
			db = redis.GetDatabase();

			try
			{
				//create the consumer group
				db.StreamCreateConsumerGroup("event_stream", "group1", "$");
			} catch
            {
				//do nothing, only trap if the group has been created
            }


		}

		//Write one entry to the Redis Streams
		public String WriteToStreams(Models.KeyValueModel keyValueModel)
		{
			//write the key value pair
			var messageId = db.StreamAdd("event_stream", keyValueModel.Key, keyValueModel.Value);

			return messageId;
		}

		//Read multiple values using ConsumerGroups thus, you can only retrieve the messages once.
		public StreamDataModel ReadBatchFromStreams(int Count = 1)
		{
			var consumer_messages/*StreamEntry[]*/ = db.StreamReadGroup("event_stream", "group1", "consumer_1", ">", count: Count);

			StreamDataModel streamDataModel = new StreamDataModel();

			if (consumer_messages.Length == 0) return null;

			int i = 0;
			Array.Resize(ref streamDataModel.streamData, consumer_messages.Length);
			foreach (var streamEntry in consumer_messages)
            {
				//streamDataModel.streamData[i] = new StreamData();
				//streamDataModel.streamData[i].Id = streamEntry.Id.ToString();
				streamDataModel.streamData[i] = new StreamData
                {
                    Id = streamEntry.Id.ToString()
                };

                streamDataModel.streamData[i].Data.Key = streamEntry.Values[0].Name;
				streamDataModel.streamData[i].Data.Value = streamEntry.Values[0].Value;
				streamDataModel.streamData[i].Data.Raw = streamEntry.Values[0].ToString();
				i++; 
			}

			streamDataModel.Count = streamDataModel.streamData.Length;

			return streamDataModel;
		}

		//Read and reread entries from the streams using the id
		public StreamDataModel ReadFromStreams(String id)
        {
			//var message = db.StreamRange("event_stream", minId: "0-0", maxId: "+", count: 1, messageOrder: Order.Ascending);

			var message/*StreamEntry[]*/ = db.StreamRead("event_stream", id);

			StreamDataModel streamDataModel = new StreamDataModel();

			if (message.Length == 0) return null;

			Array.Resize(ref streamDataModel.streamData, 1);
            streamDataModel.streamData[0] = new StreamData
            {
                Id = message[0].Id.ToString()
            };

            streamDataModel.streamData[0].Data.Key = message[0].Values[0].Name;
			streamDataModel.streamData[0].Data.Value = message[0].Values[0].Value;
			streamDataModel.streamData[0].Data.Raw = message[0].Values[0].ToString();

			streamDataModel.Count = streamDataModel.streamData.Length;

			return streamDataModel;

		}

		//Delete an entry from the stream
		public long DeleteFromStreams (string id)
        {
			//RedisValue[] redisValues = { "1597085670995-0" };
			RedisValue[] redisValues = { id };

			long DelCount = db.StreamDelete("event_stream", redisValues);

			return DelCount;

		}
	}
}
