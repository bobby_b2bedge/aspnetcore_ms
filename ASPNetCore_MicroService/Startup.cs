﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;

namespace ASPNetCore_MicroService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public RedisStreams ContigureRedis ()
        {
            String ipList = Environment.GetEnvironmentVariable("REDIS_SERVERS");

            RedisStreams redisStreams = new RedisStreams(ipList);

            redisStreams.InitStreams();

            return redisStreams;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            RedisStreams redisStreams =  ContigureRedis();
            Globals.redisStreams = redisStreams;

            /*
           Models.KeyValueModel keyValueModel = new Models.KeyValueModel();
           keyValueModel.Key = "key1";
           keyValueModel.Value = "value1";
           redisStreams.WriteToStreams(keyValueModel);
           keyValueModel.Key = "key2";
           keyValueModel.Value = "value2";
           redisStreams.WriteToStreams(keyValueModel);
           keyValueModel.Key = "key3";
           keyValueModel.Value = "value3";
           redisStreams.WriteToStreams(keyValueModel);
           keyValueModel.Key = "key4";
           keyValueModel.Value = "value4";
           redisStreams.WriteToStreams(keyValueModel);
           keyValueModel.Key = "key5";
           keyValueModel.Value = "value5";
           redisStreams.WriteToStreams(keyValueModel);

           Models.StreamDataModel streamDataModel = new Models.StreamDataModel();
           streamDataModel = redisStreams.ReadBatchFromStreams(2);
            */

            Console.WriteLine("App started");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
