﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ASPNetCore_MicroService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HelloWorldController : ControllerBase
    {
        // GET api/helloworld
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            Console.WriteLine("Get HelloWorld");
            return new string[] { "Hello World 01", "Hello world 02", "Hello world 03" };
        }

        // GET api/helloworld/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "Hello World with id = " + id;
        }

        // POST api/helloworld
        [HttpPost]
        public IActionResult Post([FromBody] string value)
        {
            //return new string[] {"same value" };
            //return Created("xxx","jkjhkj");
            return Ok("Added " + value);
        }

        // PUT api/helloworld/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] string value)
        {
            Console.WriteLine("Out for id: " + id + " value = " + value);
            return Ok();
        }

        // DELETE api/helloworld/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Console.WriteLine("Id to delete: " + id);
            return Ok();
        }
    }
}
