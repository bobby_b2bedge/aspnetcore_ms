﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASPNetCore_MicroService.Models;
using Microsoft.AspNetCore.Mvc;

namespace ASPNetCore_MicroService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StreamDataController : ControllerBase
    {
        public StreamDataController()
        {

        }

        // GET: api/streamdata
        //The values returned by the endpoint uses ConsumerGroups thus, you can only retrieve the messages once
        [HttpGet]
        public ActionResult<StreamDataModel> Get([FromQuery(Name = "MaxCount")] int maxCount = 1)
        {
            StreamDataModel streamDataModel;
            RedisStreams redisStreams = Globals.redisStreams;

            streamDataModel = redisStreams.ReadBatchFromStreams(maxCount);

            if (streamDataModel == null) return NotFound();

            return streamDataModel;
        }

        // GET api/streamdata/55555555
        //retrieve a specific message id
        [HttpGet("{id}")]
        public ActionResult<StreamDataModel> GetByID(String id)
        {
            StreamDataModel streamDataModel;
            RedisStreams redisStreams = Globals.redisStreams;

            streamDataModel = redisStreams.ReadFromStreams(id);

            if (streamDataModel == null) return NotFound();

            return streamDataModel;
        }

        // POST api/<ValuesController>
        [HttpPost]
        public IActionResult Post([FromBody] KeyValueModel value)
        {
            RedisStreams redisStreams = Globals.redisStreams;

            string id = redisStreams.WriteToStreams(value);

            return Ok(id);
        }

        // DELETE api/<ValuesController>/id
        [HttpDelete("{id}")]
        public IActionResult Delete(String id)
        {
            RedisStreams redisStreams = Globals.redisStreams;

            long DelCount = redisStreams.DeleteFromStreams(id);

            if (DelCount == 0) 
                return NotFound();
            else 
                return Ok();
        }


    }
}

