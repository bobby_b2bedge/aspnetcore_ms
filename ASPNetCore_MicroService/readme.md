ASP.net Core MicroService 
--------------------------

 

This project features two simple APIs

### Helloworld 

A simple example of using GET,POST,PUT,DELETE in ASP.net Core

 

### StreamData

CRUD (GET,POST,DELETE) functionality for Redis via API. Update has not been implemented

 

### To get started 

git clone this repo:

`git clone https://bitbucket.org/bobby_b2bedge/aspnetcore_ms.git`

 

The either load the solution and run the project directly from Visual Studio 2019. Docker support needs to be installed in Visual Studio first.

or 

execute:

Login to docker (if not already done so)
`docker login -u <account> -p <password>`

To build the docker image 
Note: Execute the docker command from the root project root folder (aspnetcore_ms)

`docker build --force-rm -t aspnetcoremicroservice:dev  .`

To execute it in docker:

`docker run -dt -p 7080:80 -p 7443:443 -e REDIS_SERVERS=<redis ip address>:6379 --name ASPNetCore_MicroService aspnetcoremicroservice:dev`

Please make sure that your Redis server is started before running the container. Also update the environment variable `REDIS_SERVERS` to point to your Redis Server. If running locally in docker use the machine IP address and not 127.0.0.1 as it might not work well.


To execute it in Kubernetes

`docker build --force-rm -t <acount>/aspnetcoremicroservice:dev  .`

Push the docker image to a docker container repository

`docker push <account>/aspnetcoremicroservice:dev`

Create the service (of type NodePort) to allow external access to the pod. This is valid for one node running multiple replicas (pods).
`kubectl apply -f kubernetes-service.yaml`

Set the environment variables. For example the the redis db IP and port. See below note
`kubectl apply -f kubernetes-envvars.yaml`

Deploy the container
`kubectl delete deployment aspnetcoremicroservice`

`kubectl apply -f kubernetes-deployment.yaml`

Please make sure that your Redis server is started before running the container. Also update the environment variable `REDIS_SERVERS` in kubernetes-envvars.yaml to point to your Redis Server. Do not use localhost or 127.0.0.1, use the machine IP.

To get apisix running
Go to folder APISIX and execute:
docker-compose -p docker-apisix up -d

Follow the standard procedure to add services and 
IP of service - use the IP where the docker or kubernetes is running
post of service - use the port where the docker (7080) or kubernetes (30080) is running

curl http://<APISIX_IP>:9080/apisix/admin/services/ -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "upstream": {
        "type": "roundrobin",
        "nodes": {
            "<IP of service>:<post of service>": 1
        }
    }
}'
results are something like this:
{
    "header": {
        "raft_term": "11",
        "cluster_id": "14841639068965178418",
        "member_id": "10276657743932975437",
        "revision": "98"
    },
    "node": {
        "key": "/apisix/services/00000000000000000097",
        "value": {
            "upstream": {
                "hash_on": "vars",
                "pass_host": "pass",
                "nodes": {
                    "192.168.3.58:7080": 1
                },
                "type": "roundrobin"
            }
        }
    },
    "action": "create"
}
Please note the id 00000000000000000097 as a service_id. Plese use it when creating the route

curl http://<APISIX_IP>:9080/apisix/admin/routes/ -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/*",
    "host": "192.168.3.58",
    "service_id": "00000000000000000097"
}'
results are something like this:
{
    "header": {
        "raft_term": "11",
        "cluster_id": "14841639068965178418",
        "member_id": "10276657743932975437",
        "revision": "99"
    },
    "node": {
        "key": "/apisix/routes/00000000000000000098",
        "value": {
            "host": "192.168.3.58",
            "service_id": "00000000000000000097",
            "uri": "/*",
            "priority": 0
        }
    },
    "action": "create"
}

To test do an api call to APISIX IP and port:9080

To test the solution checkout the postman collection to test the APIs.

`git clone https://bitbucket.org/bobby_b2bedge/aspnetcore_ms_addons.git`


