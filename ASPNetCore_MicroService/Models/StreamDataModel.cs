﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPNetCore_MicroService.Models
{
    public class StreamDataModel
    {
        public StreamDataModel()
        {
            //streamData = new StreamData[1];
        }

        public int Count { get; set; }
        public StreamData[] streamData;
    }

    public class StreamData
    {
        public StreamData()
        {
            Data = new KeyValueModel();
        }

        public String Id { get; set; }
        public KeyValueModel Data { get; set; }
    }

    public class KeyValueModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string Raw { get; set; }
    }
}
